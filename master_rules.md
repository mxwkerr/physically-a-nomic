# Physically a Nomic

## `1` Rule Changes
- `1.0` The rules of this Nomic can be changed whenever a player decides to change them and all players consent to the change.

## `2` Hit points
- `2.0` All players get 10 hit points.
